﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CevreWebSitesi.Startup))]
namespace CevreWebSitesi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
